# GATOR Front-End Interface

Front end interface to test different model configurations.

# Installation

## Setting up the conda environment
If you do not have a gator conda environment set up yet, do the following to set one up: 
```bash
conda create -n gator python=3.8
```

Then, activate your environment like this:
```bash
conda activate gator
```

Install pip and pip requirements:
```bash
conda install pip

which pip # should be /Users/<username>/opt/miniconda3/envs/gator/bin/pip

pip install -r requirements.txt
```

## Dependencies

 * [Python 3.8](https://docs.python.org/3.8/)
 	* Flask==1.1.2
	* Flask-Cors==3.0.9
	* Flask-Socketio==4.3.1

---
