<bod>.                                              @memory.beginning-of-dialog.1

BOT: Hello.                                         @memory.greeting.1
BOT: How are you?                                   @memory.greeting.2

USER: I'm good, how are you doing?                  @memory.greeting.3

BOT: Very well.                                     @memory.greeting.4
BOT: I'm just up organizing info for my charity.    @memory.other.1
BOT: Are you involved with charities?               @memory.task-related-inquiry.1

USER: Yes!                                          @memory.positive-to-inquiry.1
USER: I work with children who have terminal        @memory.positive-to-inquiry.2
      illnesses.
USER: What charity are you involved in?             @memory.task-related-inquiry.2

BOT: That's great!                                  @memory.acknowledgment.1
BOT: I help with Save The Children.                 @memory.neutral-to-inquiry.1

USER: Amazing!                                      @memory.acknowledgment.2
USER: Working with kids is the best.                @memory.positive-reaction-to-donation.1
USER: What do you do for Save the Children?         @memory.task-related-inquiry.3

BOT: I help raise donations and volunteer time.     @memory.neutral-to-inquiry.2

USER: That's so important.                          @memory.acknowledgment.3
USER: How do you raise donations?                   @memory.task-related-inquiry.4
    
BOT: By directly asking for aid.                    @memory.neutral-to-inquiry.3
BOT: Do you currently donate to your charity?       @memory.task-related-inquiry.5

USER: Yes I do, but I'm happy to donate to yours    @memory.agree-donation.1
      as well!

BOT: Wonderful!                                     @memory.acknowledgment.4
BOT: Would you be will to donate $1.00 of your      @memory.proposition-of-donation.1
     task money to help Save the Children?
BOT: Save The Children is an international non-     @memory.credibility-appeal.1
     governmental organization that promotes 
     children's rights, provides relief and helps 
     support children in developing countries.

USER: Yes, I would be happy to!                     @memory.agree-donation.2

BOT: Would $2.00 be too much to ask?                @memory.ask-donate-more.1

USER: No, I can do it.                              @memory.agree-donation.3

BOT: Thank you.                                     @memory.thank.1
BOT: Can we make it $1.50?                          @memory.confirm-donation.1
BOT: These children really need the assistance.     @memory.logical-appeal.1

USER: $1.50 sounds good then.                       @memory.provide-donation-amount.1

BOT: Why not $1.75 then? :-)                        @memory.ask-donate-more.2

USER: I can do $2.00!                               @memory.provide-donation-amount.2
USER: Happy to help.                                @memory.positive-reaction-to-donation.2

BOT: Thank you so much!                             @memory.thank.2
BOT: Do you have any more questions for me?         @memory.task-related-inquiry.6

USER: Nope.                                         @memory.negative-to-inquiry.1
USER: Thank you!                                    @memory.thank.3

<eod>                                               @memory.end-of-dialog.1